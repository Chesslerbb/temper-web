import Vue from 'vue'
import HighcartsVue from 'highcharts-vue'
import App from './App.vue'

Vue.config.productionTip = false

Vue.use(HighcartsVue)

new Vue({
  render: h => h(App),
}).$mount('#app')
